# Drama client resurrected

```mermaid
graph TB;
  0(start) --> 1[get current working URL];
  1 --> 2[get user input];
  2 --> 3[parse website for user input];
  3 --> 4{found titles by user input?};
  4 -- no --> 4.1[error message];
  4.1 -->2;
  4 -- yes --> 5[list titles & ask for input];
  5 --> 5.1{number exists in the list?};
  5.1 -- no --> 5.2[error message];
  5.2 --> 5;
  5.1 -- yes --> 6[print number of episodes & ask for user input];
  6 --> 6.1{episode exists?}
  6.1 -- no --> 6.2[error];
  6.2 --> 6;
  6.1 -- yes --> 7[get url of episode];
  7 --> 8[pass url as argument for vlc];
  8 --> 9(end);

```